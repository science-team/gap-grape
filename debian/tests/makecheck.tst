## debian/tests/makecheck.tst -- GAP Test script
## script format: GAP Reference Manual section 7.9 Test Files (GAP 4r8)
##
gap> TestPackageAvailability( "grape" , "=4.9.2" , true );
"/usr/share/gap/pkg/grape/"

##
## eos
